/**
 * Created by mattritter on 4/4/17.
 */
var path = require('path');

module.exports = {
    entry: {
        //dragdropdirective: './lib/drag-drop-directive.module.ts',
        index: './lib/index.ts'
    },
    output: {
        library: 'angular4-drag-drop',
        libraryTarget: 'umd',
        filename: '[name].bundle.js',
        path: path.resolve(__dirname, './dist')
    },
    resolve: {
        extensions: ['.ts'],
    },
    // you will need the ts-loader modules so that webpack knows what to do with typescript files.
    module: {
        rules: [
            { 
                test: /\.ts$/, 
                exclude: /node_modules/, 
                loaders: 'ts-loader',
                options: {
                    configFileName: 'tsconfig.lib.json'
                }
            }
        ]
    }
};