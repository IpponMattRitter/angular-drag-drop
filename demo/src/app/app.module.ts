import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { DragDropDirectiveModule} from "../../../dist/";
import { AppComponent } from './app.component';
import { DropAreaComponent } from './drop-area/drop-area.component';
import { NavigationComponent } from './navigation/navigation.component';
import { GenericBoxComponent } from './generic-box/generic-box.component';

@NgModule({
  imports: [
    BrowserModule,
    DragDropDirectiveModule
  ],
  declarations: [
    AppComponent,
    DropAreaComponent,
    NavigationComponent,
    GenericBoxComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
