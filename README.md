# Drag and drop attribute directive for Angular 4+

An angular4 modules that exports a drag and a drop attribute directives. 
This probably doesnt work yet.

## Table of Contents

- [Installation](#installation)
- [Usage](#usage)
- [Support](#support)

## Installation
```
$ npm i angular4-drag-drop --save
```


## Usage
To import the module into a module in which you wish to use the directive on components.
```
import { DragDropDirectiveModule} from "angular4-drag-drop/lib/drag-drop-directive.module";

```
This module exports two attribute directives, 'DraggableDirective' and 'DroppableDirective'.  DraggableDirectve is used with a component from which you want to drag items, while DroppableDirective is used with the component with which you wish to drop items into.  Both accept inputs for CSS class highlighting. Both will emit and the dragged item when it is dropped to pop or push into an array, error check, or whatever else you wish to do with that item.
### DragDirective
```typescript
<div *ngFor="let item of itemsToDrop" [dragDirective]='item' [dragHightlight]="'highlight'" (releaseDrop)="releaseDrop($event)">
</div>
```
'[dragDirective]="item"' apply the drag directive, and pass it an item.  '[dragHighlight]="'highlight'"' passes a string that add to the element css class list.  '(releaseDrop)="releaseDrop($event)"' calls a function to act on the dragged item once it is dropped. 

### DropDirectve
```typescript
<div dropDirective (dropEvent)="addDropItem($event)" class="droppable" [dropHighlight]="'highlight'" >
</div>
```
'dropDirective' applies the directive to an element, making it a drop target.  '[dropHighlight]="'highlight'"' passes a string to add a css class to the css class list of the element.  '(dropEvent)="addDropItem($event)"' calls a function and passes the dropped item.
