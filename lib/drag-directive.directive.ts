import { Directive, ElementRef, HostListener, Input, Output, EventEmitter} from '@angular/core';
import { DragDropDirectiveService } from './drag-drop-directive.service';
import { Subscription } from 'rxjs/Subscription';

@Directive({
  selector: '[dragDirective]'
})
export class DragDirective{
  @Input('dragDirective')
  draggedItem:Object;
  @Input('dragHightlight')
  cssHighlight:string;
  @Output()
  releaseDrop:EventEmitter<any> = new EventEmitter();
  
	private highlighted:boolean = false;
  private dropSubscription: Subscription;

  constructor(
    private el: ElementRef,
    private dragDropDirectiveService: DragDropDirectiveService
   ) {
  	this.el.nativeElement.draggable='true';    
  }
  @HostListener('mouseenter') onMouseEnter() {
  	this.highlighted = true;
    this.highlight();
  }
  @HostListener('mouseout') onMouseOut(){
  	this.highlighted = false;
  	this.highlight();
  }
  @HostListener('dragstart',['$event']) onDragStart(event:any){
    // html draggable will not transfer an object, so we stringify it
    let transferObject = JSON.stringify(this.draggedItem);
    event.dataTransfer.setData("customObject",transferObject);
    this.dropSubscription = this.dragDropDirectiveService.getDropItem().subscribe(
      item => {
        this.emitDraggedItem(this.draggedItem);
      }
    )
  }

  private emitDraggedItem(item){
    this.releaseDrop.emit(item);
    this.dropSubscription.unsubscribe();
  }

  private highlight(){
  	if (this.highlighted){
  	 this.el.nativeElement.classList.add(this.cssHighlight);
  	} else {
  		this.el.nativeElement.classList.remove(this.cssHighlight);
  	}
  }
}
