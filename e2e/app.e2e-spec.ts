import { AngularDraggablePage } from './app.po';

describe('angular-draggable App', () => {
  let page: AngularDraggablePage;

  beforeEach(() => {
    page = new AngularDraggablePage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
