var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
var DragDropDirectiveService = (function () {
    function DragDropDirectiveService() {
        this.dropItem = new Subject();
    }
    DragDropDirectiveService.prototype.setDropItem = function (item) {
        this.dropItem.next(item);
    };
    DragDropDirectiveService.prototype.getDropItem = function () {
        return this.dropItem.asObservable();
    };
    return DragDropDirectiveService;
}());
DragDropDirectiveService = __decorate([
    Injectable(),
    __metadata("design:paramtypes", [])
], DragDropDirectiveService);
export { DragDropDirectiveService };
//# sourceMappingURL=drag-drop-directive.service.js.map