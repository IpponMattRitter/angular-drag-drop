var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Directive, ElementRef, HostListener, Input, Output, EventEmitter } from '@angular/core';
import { DragDropDirectiveService } from './drag-drop-directive.service';
var DropDirective = (function () {
    function DropDirective(el, dragDropDirectiveService) {
        this.el = el;
        this.dragDropDirectiveService = dragDropDirectiveService;
        this.dropEvent = new EventEmitter();
        this.highlighted = false;
    }
    DropDirective.prototype.onDragEnter = function () {
        this.highlighted = true;
        this.highlight();
    };
    DropDirective.prototype.onDragLeave = function () {
        this.highlighted = false;
        this.highlight();
    };
    DropDirective.prototype.onDragOver = function () {
        event.preventDefault();
    };
    DropDirective.prototype.onDrop = function (event) {
        // since html draggable will not transer an object, we need to parse are string
        var transferredObject = JSON.parse(event.dataTransfer.getData('customObject'));
        var transferredObjectID = event.dataTransfer.getData('customID');
        this.dropEvent.emit(transferredObject);
        this.dragDropDirectiveService.setDropItem(transferredObjectID);
    };
    DropDirective.prototype.highlight = function () {
        if (this.highlighted) {
            this.el.nativeElement.classList.add(this.cssHighlight);
        }
        else {
            this.el.nativeElement.classList.remove(this.cssHighlight);
        }
    };
    return DropDirective;
}());
__decorate([
    Input('dropHighlight'),
    __metadata("design:type", String)
], DropDirective.prototype, "cssHighlight", void 0);
__decorate([
    Output(),
    __metadata("design:type", EventEmitter)
], DropDirective.prototype, "dropEvent", void 0);
__decorate([
    HostListener('dragenter'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], DropDirective.prototype, "onDragEnter", null);
__decorate([
    HostListener('dragleave'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], DropDirective.prototype, "onDragLeave", null);
__decorate([
    HostListener('dragover'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], DropDirective.prototype, "onDragOver", null);
__decorate([
    HostListener('drop', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], DropDirective.prototype, "onDrop", null);
DropDirective = __decorate([
    Directive({
        selector: '[dropDirective]'
    }),
    __metadata("design:paramtypes", [ElementRef,
        DragDropDirectiveService])
], DropDirective);
export { DropDirective };
//# sourceMappingURL=drop-directive.directive.js.map