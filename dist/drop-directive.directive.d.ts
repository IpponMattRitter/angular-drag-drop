import { ElementRef, EventEmitter } from '@angular/core';
import { DragDropDirectiveService } from './drag-drop-directive.service';
export declare class DropDirective {
    private el;
    private dragDropDirectiveService;
    cssHighlight: string;
    dropEvent: EventEmitter<Object>;
    private highlighted;
    constructor(el: ElementRef, dragDropDirectiveService: DragDropDirectiveService);
    onDragEnter(): void;
    onDragLeave(): void;
    onDragOver(): void;
    onDrop(event: any): void;
    private highlight();
}
