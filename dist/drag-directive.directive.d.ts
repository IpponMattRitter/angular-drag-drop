import { ElementRef, EventEmitter } from '@angular/core';
import { DragDropDirectiveService } from './drag-drop-directive.service';
export declare class DragDirective {
    private el;
    private dragDropDirectiveService;
    draggedItem: Object;
    cssHighlight: string;
    releaseDrop: EventEmitter<any>;
    private highlighted;
    private dropSubscription;
    constructor(el: ElementRef, dragDropDirectiveService: DragDropDirectiveService);
    onMouseEnter(): void;
    onMouseOut(): void;
    onDragStart(event: any): void;
    private emitDraggedItem(item);
    private highlight();
}
