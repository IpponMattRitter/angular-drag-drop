import { Observable } from 'rxjs/Observable';
export declare class DragDropDirectiveService {
    private dropItem;
    constructor();
    setDropItem(item: any): void;
    getDropItem(): Observable<any>;
}
